<?php

use App\Http\Controllers\AuthenticationController;
use App\Http\Controllers\CommitteeController;
use App\Http\Controllers\EventController;
use App\Http\Controllers\TicketController;
use Illuminate\Support\Facades\Route;

Route::middleware(['web'])->group(function () {
    Route::get('/', function () {
        return view('welcome');
    })->name('home');

    Route::get('login', [AuthenticationController::class, 'showLogin'])->name('login');
    Route::post('login', [AuthenticationController::class, 'authenticate'])->name('login');
    Route::post('logout', [AuthenticationController::class, 'logout'])->name('logout');
    if (app()->environment('local')) {
        Route::get('login/as/{id}', [AuthenticationController::class, 'login_as'])->name('login_as');
    }

    // Committee routes
    Route::resource('committees', CommitteeController::class);

    // Event routes
    Route::resource('events', EventController::class);

    // Ticket routes
    Route::resource('tickets', TicketController::class);
    // TODO: add the other routes
});
