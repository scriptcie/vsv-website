@extends('layout.base')

@section('title', $event->title)

@section('main')

    <h1>{{ $event->title }}</h1>
    <p>{{ $event->description }}</p>
    @isset($event->location)
        <p>The event will take place at {{ $event->location }}</p>
        <br>
    @endisset

    <p>Select your options for {{ $event->title }} </p>
    <br>
    {{-- TODO: change route to tickets.create --}}
    <form action="{{ route('home') }}" method="get">
        @csrf
        <input type="hidden" name="step" value="1">
        <input type="hidden" name="event_id" value="{{ $event->id }}">

        @include('tickets.options')

        <button type="submit" class="btn btn-primary">Next</button>
    </form>

@endsection
