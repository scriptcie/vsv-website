@extends('layout.base')

@section('title', 'Events')

@section('main')

    <h1>Events</h1>

    <table class="table table-borderless table-striped">
        <thead>
            <tr>
                <th>Title</th> {{-- TODO: do we want to show this? --}}
                @if (auth()->check() && (auth()->user()->hasRole('admin') || auth()->user()->hasRole('board')))
                    <th>Committee Name</th>
                    <th>Status</th>
                @endif
                <th class="text-end">Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($events as $item)
                <tr>
                    <td>
                        <a href="{{ route('events.show', $item->id) }}" class="btn event-title">{{ $item->title }}</a>
                    </td>
                    @if (auth()->check() && (auth()->user()->hasRole('admin') || auth()->user()->hasRole('board')))
                        <td>
                            <a href="#{{-- TODO: add route --}}" class="btn event-title">{{ $item->committee->name }}</a>
                        </td>
                        <td>
                            {{-- TODO: Add counts --}}
                            Checked In: {{ 0 }} <br>
                            Remaining: {{ 0 }}
                        </td>
                    @endif
                    <td>
                    @if (auth()->check() && (auth()->user()->hasRole('board') || auth()->user()->hasRole('admin')))
                        <a href="{{-- TODO: add route --}}" class="btn btn-secondary position-relative">
                            Show
                            tickets
                            <span class="badge text-bg-secondary bg-danger rounded-circle">
                                {{ 123}}
                                {{-- {{ $item->tickets->count() }} --}}
                                {{-- TODO: see line above --}}
                            </span>
                        </a>
                    @endif
                    @if (auth()->check() && (auth()->user()->hasRole('board') || auth()->user()->hasRole('admin')))
                        @can('update', $item)
                            <a href="{{ route('events.edit', $item->id) }}" class="btn btn-primary">
                                <i class="fas fa-edit"></i>
                            </a>
                        @endcan
                        <a href="{{ route('events.show', $item->id) }}" class="btn btn-primary position-relative">
                            Book Now
                        </a>
                    @endif
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>

@endsection
