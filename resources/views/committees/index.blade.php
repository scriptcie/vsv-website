@extends('layout.base')

@section('title', 'Committees')

@section('main')
    <h1>Committees</h1>

    <ul>
        @foreach ($committees as $item)
            <li>
                {{-- TODO: add pictures --}}

                @auth
                    <a href="{{ route('committees.show', $item) }}">
                        {{ $item->abbreviation }}
                    </a>
                @else
                    {{ $item->abbreviation }}
                @endauth
            </li>
        @endforeach
    @endsection
