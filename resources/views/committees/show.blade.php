@extends('layout.base')

@section('title', $committee->name)

@section('main')

    <h1>{{ $committee->name }}</h1>

    @php
      $users = $committee->users->sortBy('pivot.rank', true);
      $users = $users->map(function($user) {
        return [
            'id' => $user->id,
            'function' => $user->pivot->function,
            'rank' => $user->pivot->rank,
            'year' => $user->pivot->year,
        ];
      });
    @endphp

    <table class="table table-striped">
        <thead>
            <tr>
                <th scope="col">Name</th>
                <th scope="col">Function</th>
                {{-- TODO: Make this actually show the people --}}
            </tr>
        </thead>
    </table>

@endsection
