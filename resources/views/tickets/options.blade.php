<div class="form-floating">
    <select id="baseoption" class="form-control" name="baseoption" id="">
        @if ($event->options->where('base', 1)->count() == 1)
            @php
                $option = $event->options->where('base1', 1)->first();
            @endphp
            <option data-price="@if (isset($option->price)) {{ $option->price }} @else 0 @endif}"
                value="{{ $option->id }}">
                {{ $option->name }} - @if (isset($option->price))
                    €{{ $option->price }}
                @else
                    Free
                @endif
            </option>
        @else
            @php
                $selectedoption = request()->get('baseoption');

            @endphp
            <option disabled selected>Please select your ticket option...</option>
            @foreach ($event->options->where('base', 1) as $option)
                <option {{ $selectedoption == $option->id ? 'selected' : '' }}
                    data-price="@if (isset($option->price)) {{ $option->price }} @else 0 @endif}"
                    value=" {{ $option->id }}" @if ($option->id == ($request->baseoption ?? null)) selected @endif>
                    {{ $option->name }} - @if (isset($option->price))
                        €{{ $option->price }}
                    @else
                        Free
                    @endif
                </option>
            @endforeach
        @endif
    </select>
    <label for="baseoption">Ticket option</label>
</div>
<br>
@if ($event->options->where('base', 0)->count() != 0)
    <label for="options">Additional options:</label><br>
    @foreach ($event->options->where('base', 0) as $option)
        <div class="form-check">
            <input class="form-check-input" type="checkbox" id="{{ $option->name }}" name="options[]"
                data-price="{{ $option->price }}" value="{{ $option->id }}"
                @if (in_array($option->id, $request->options ?? [])) checked @endif />
            <label class="form-check-label" for="{{ $option->name }}">
                {{ $option->name }} - €{{ $option->price }}
            </label>
        </div>
    @endforeach
@endif
<span id="price">No price</span><br><br>

@section('scripts')
    <script>
        function price() {
            let price = parseFloat($("select[name=baseoption]").find(':selected').data('price'));
            $.each($("input[name='options[]']:checked"), function(key, value) {
                price += parseFloat($(value).data('price'));
            });
            if (isNaN(price)) {
                price = "Select ticket";
            } else {
                price = "Your ticket will cost €" + Math.round(price * 100) / 100 + ".";
            }
            $("#price").text(price);
        }
        $("select[name=baseoption]").on("change", price);
        $("input[name='options[]']").on("change", price);
        price();
    </script>
@endsection
