<nav class="navbar navbar-expand-lg bg-dark" data-bs-theme="dark">
    <div class="container-fluid">
        {{-- LOGO --}}
        <a class="navbar-brand" href="{{ route('home') }}">
            <img class="logo" src="{{ asset('img/logo_white.png') }}" alt="VSV Logo">
        </a> {{-- TODO: Change Href to correctly point to the home page --}}

        {{-- Hamburger Button (mobile/small screens) --}}
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navigation"
            aria-controls="navigation" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        {{-- Navigation items --}}
        <div class="collapse navbar-collapse" id="navigation">
            <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown">
                        <i class="fas fa-users"></i>
                        Committees

                    </a> {{-- TODO: Add links --}}
                    <ul class="dropdown-menu submenu">
                        <li>
                            <a class="dropdown-item" href="{{ route('committees.index') }}">
                                Overview
                            </a>
                        </li>
                        <li>
                            <a class="dropdown-item" href="#">
                                Add new
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown">
                        <i class="fas fa-ticket-alt"></i>
                        Events

                    </a> {{-- TODO: Add links --}}
                    <ul class="dropdown-menu submenu">
                        <li>
                            <a class="dropdown-item" href="{{ route('events.index') }}">
                                Overview
                            </a>
                        </li>
                        <li>
                            <a class="dropdown-item" href="#">
                                Add new
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown">
                        <i class="fas fa-users"></i>
                        Committees

                    </a> {{-- TODO: Add links --}}
                    <ul class="dropdown-menu submenu dropdown-menu-right">
                        <li>
                            <a class="dropdown-item" href="#">
                                Overview
                            </a>
                        </li>
                        <li>
                            <a class="dropdown-item" href="#">
                                Add new
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>
