<header>
    <div class="navbar-login py-0 header-top fixed-top">
        <ul class="navbar-nav ms-auto mb-2 mb-lg-0 nav-login">

            @auth

            <li>
                <a class="register-btn" href="#"> Logged in as {{ Auth::user()->name }} </a>
            </li>
            <li>
                {{-- <a class="login-btn" href="{{ route('logout') }}">
                    Log out
                </a> --}}
                <form action="{{ route('logout') }}" method="post">
                    @csrf
                    <button class="login-btn" type="submit">
                        Logout
                    </button>
                </form>
            </li>

            @else

            <li>
                <a class="register-btn" href="#">
                    Not a member yet? Click here to register!
                </a>
            </li>
            <li>
                <a class="login-btn" href="{{ route('login', ['redirect' => url()->current()]) }}">
                    Login
                </a>
            </li>

            @endauth

        </ul>
    </div>
</header>
