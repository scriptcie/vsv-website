<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    {{-- Title --}}
    <title>
        {{ config('app.name') }}
        @hasSection('title')
            - @yield('title')
        @endif
    </title>

    {{-- Styles and Scripts --}}
    @vite(['resources/sass/app.scss', 'resources/css/app.css', 'resources/js/app.js'])
    @yield('styles')

</head>
<body class="d-flex flex-column min-vh-100">

    @include('layout.topbar')

    @include('layout.navbar')

    <main>
        <div class="container my-5">
            @include('layout.error')
            @include('layout.success')
            @yield('main')
        </div>
    </main>

    @include('layout.footer')

    {{-- Scripts --}}
    {{-- TODO: Check if we need a better way of incorporating jquery --}}
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"
        integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
        crossorigin="anonymous"></script>
    @yield('scripts')
</body>
</html>
