<footer class="mt-auto py-3 bg-dark">
    <div class="container">
        <div class="col-sm-12">
            <div class="row">
                <div class="col-lg-3 col-md-6 mb-5">
                    <h4 class="title">Contact</h4>
                    <p>
                        VSV 'Leonardo da Vinci'<br>
                        Faculteit LR, TU Delft<br>
                        Kluyverweg 1<br>
                        2629 HS Delft<br>
                        015 27 85366<br>
                        <a href="mailto:vsv@tudelft.nl">vsv@tudelft.nl</a><br>
                        KVK 40397684<br>
                    </p>
                </div>
                <div class="col-lg-3 col-md-6 mb-5">
                    <h4 class="title"><a class="rsswidget" href="https://www.flightglobal.com/rss/news/"><img
                                class="rss-widget-icon" src="{{ asset('img/rss.png') }}" alt="RSS"></a>
                        <a href="#"> Aviation news</a>
                    </h4>
                </div>
                <div class="col-lg-3 col-md-6 mb-5">
                    <h4 class="title"><a class="rsswidget" href="https://www.flightglobal.com/rss/news/"><img
                                class="rss-widget-icon" src="{{ asset('img/rss.png') }}" alt="RSS"></a>
                        <a href="#"> Aviation news 2</a>
                    </h4>
                </div>
                <div class="col-lg-3 col-md-6 mb-5"></div>


                <div class="col-sm-12 copyright">
                    Copyright © 1945-2015 VSV 'Leonardo da Vinci'. All Rights Reserved.
                </div>


            </div>
        </div>
    </div>
    </div>
</footer>
