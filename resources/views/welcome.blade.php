@extends('layout.base')

@section('title', 'Home')

@section('main')
    <div class="row">
        <div class="col-sm-12">
            <h1>Welcome to the VSV Website</h1>
            <p>
                This is the website of the VSV 'Leonardo da Vinci'. The VSV is the study association for the Aerospace Engineering faculty at the TU Delft.
            </p>

            <p>
                On this website you can find information about the VSV, our activities, and our partners. You can also find information about the study Aerospace Engineering at the TU Delft.
            </p>

        </div>
    </div>
@endsection
