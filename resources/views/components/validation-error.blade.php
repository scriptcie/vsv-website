@props(['key'])
@error($key)
    <p {{ $attributes->merge(['class' => 'text-danger m-0']) }} >{{ $message }}</p>
@enderror
