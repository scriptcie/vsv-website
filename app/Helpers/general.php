<?php

// Gets initials from a name

use BaconQrCode\Renderer\Image\ImagickImageBackEnd;
use BaconQrCode\Renderer\ImageRenderer;
use BaconQrCode\Renderer\RendererStyle\RendererStyle;
use BaconQrCode\Writer;
use Illuminate\Support\Facades\Storage;

// Get the initials based from a name
if (! function_exists('initials')) {
    function initials($name): string
    {
        $return = '';
        foreach (explode(' ', $name) as $word) {
            $return .= sprintf('%s. ', strtoupper($word[0]));
        }

        return $return;
    }
}

// Create temporary QR code and return path of file
if (! function_exists('qr_code')) {
    function qr_code($content): string
    {
        $directory = storage_path('app/public/qrcode');

        if (! Storage::isDirectory($directory)) {
            Storage::makeDirectory($directory, 0777, true, true);
        }

        $name = time().'.png';
        $storage_path = $directory.'/'.$name;

        $temp = tempnam(sys_get_temp_dir(), 'vsv');
        $renderer = new ImageRenderer(
            new RendererStyle(400),
            new ImagickImageBackEnd()
        );
        $writer = new Writer($renderer);
        $writer->writeFile($content, $storage_path);

        return $name;
    }
}
