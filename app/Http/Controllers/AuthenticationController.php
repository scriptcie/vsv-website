<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthenticationController extends Controller
{
    public function login()
    {
        // TODO: Integrate TU SSO
    }

    public function logout(Request $request): RedirectResponse
    {
        if (! $request->isMethod('post')) {
            abort(405);
        }

        $previousUrl = url()->previous();

        Auth::logout();
        request()->session()->invalidate();
        request()->session()->regenerateToken();

        return redirect($previousUrl)->with('success', 'You successfully logged out!');
    }

    public function login_as(int $id): RedirectResponse
    {
        // Only allow login is during developments
        if (config('app.env') != 'local') {
            abort(403);
        }

        $previousUrl = session('url.intended', url()->previous());

        // First call the logout method to make sure that the current session is terminated
        Auth::logout();
        // Then create a new session using the provided identifier
        Auth::loginUsingId($id);

        // Finally redirect to the page where the user came from
        return redirect()->intended($previousUrl)->with('success', 'You successfully logged in.');
    }

    public function showLogin(Request $request)
    {
        if (! $request->session()->has('url.intended') && url()->previous() !== route('login')) {
            $request->session()->put('url.intended', url()->previous());
        }

        return view('auth.login');
    }

    public function authenticate(Request $request)
    {
        $credentials = $request->validate([
            'email' => ['required', 'email'],
            'password' => ['required'],
        ]);

        $user = User::where('email', $credentials['email'])->first();

        // TODO: Redo/Update this entire part
        if (Auth::attempt($credentials)) {
            $request->session()->regenerate();
            $user->Auth::user();

            if ($user->hasRole('admin')) {
                return redirect()->route('home');
            } elseif ($user->hasRole('board')) {
                return redirect()->route('home');
            } elseif ($user->hasRole('user')) {
                return redirect()->route('home');
            } else {
                // Handle if the user doesn't have a role
                return back()->withErrors([
                    'email' => 'The provided credentials do not match our records.',
                ]);
            }
        }

        return back()->withErrors([
            'email' => 'The provided credentials do not match our records.',
        ])->onlyInput('email');
    }
}
