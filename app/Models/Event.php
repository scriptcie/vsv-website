<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\Pivot;

class Event extends Pivot
{
    protected $table = 'events';

    protected $fillable = [
        'title',
        'description',
        'location',
        'start',
        'end',
        'committee_id',
        'capacity',
        'public',
    ];

    public function committee(): BelongsTo
    {
        return $this->belongsTo(Committee::class);
    }

    public function options(): HasMany
    {
        return $this->hasMany(EventOption::class, 'event_id');
    }

    public function fields(): HasMany
    {
        return $this->hasMany(EventField::class, 'event_id');
    }
}
