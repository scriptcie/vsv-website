<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    /** @use HasFactory<\Database\Factories\UserFactory> */
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var list<string>
     */
    protected $fillable = [
        'student_number',
        'title',
        'first_name',
        'last_name',
        'gender_id',
        'birthday',
        'country_id',
        'email',
        'phone_number',
        'address',
        'postal_code',
        'city',
        'country',
        'study_phase_id',
        'iban',
        'bic',
        'bank_account_name',
        'bank_account_city',
        'parents_email',
        'parents_phone_number',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var list<string>
     */
    protected $hidden = [
        'password',
        'remember_token',
        'email_verification_token',
    ];

    /**
     * Get the attributes that should be cast.
     *
     * @return array<string, string>
     */
    protected function casts(): array
    {
        return [
            'birthday' => 'date:Y-m-d',
            'email_verified_at' => 'datetime',
            'last_login_at' => 'datetime',
            'password' => 'hashed',
        ];
    }

    public function hasRole($roleName): bool
    {
        return $this->role === $roleName;
    }

    public function setRole($roleName): User
    {
        $this->role = $roleName;
        $this->save();

        return $this;
    }

    public function getNameAttribute(): string // $user->name
    {
        return trim(sprintf('%s %s', $this->first_name, $this->last_name));
    }

    public function getBirthdayAttribute(): string // makes sure that the birthday is always in the format 'Y-m-d' or empty sting, for consistency
    {
        return ! is_null($this->attributes['birthday']) ? Carbon::parse($this->attributes['birthday'])->format('Y-m-d') : '';
    }

    public function gender(): BelongsTo
    {
        return $this->belongsTo(Gender::class);
    }

    public function study_phase(): BelongsTo
    {
        return $this->belongsTo(StudyPhase::class);
    }

    public function committees(): BelongsToMany
    {
        return $this->belongsToMany(Committee::class, 'committee_user')->withPivot('function', 'year', 'rank');
    }

    public function committees_current(): BelongsToMany
    {
        $date = Carbon::now();
        if ($date->month <= 6) {
            return $this->committees()->wherePivot('year', $date->year - 1);
        } elseif ($date->month <= 8) {
            return $this->committees()->wherePivotIn('year', [$date->year - 1, $date->year]);
        }

        return $this->committees()->wherePivot('year', $date->year);
    }

    // public function memberships()
    // {
    //     return $this->hasMany(Membership::class);
    // }

    // public function membership()
    // {
    //     return $this->hasOne(Membership::class);
    // }
}
