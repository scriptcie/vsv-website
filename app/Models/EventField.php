<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class EventField extends Model
{
    protected $table = 'event_fields';

    protected $fillable = [
        'id',
        'name',
        'description',
        'html',
        'validation',
        'event_option_id',
    ];

    public function event(): BelongsTo
    {
        return $this->belongsTo(Event::class);
    }

    public function eventOption(): BelongsTo
    {
        return $this->belongsTo(EventOption::class);
    }
}
