<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id(); // for entire table need to look at what should and what should not be nullable
            $table->bigInteger('student_number')->unique();
            $table->string('title')->nullable();
            $table->string('initials');
            $table->string('first_name');
            $table->string('middle_name')->nullable();
            $table->string('last_name');
            $table->foreignId('gender_id')->nullable()->constrained()->nullOnDelete();
            $table->date('birthday');
            $table->string('birthplace');
            $table->foreignId('nationality_id')->constrained('countries');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('phone_number')->nullable()->unique(); // Should this be unique? AND should there also be mobile_number?
            $table->string('address')->nullable();
            $table->string('postal_code');
            $table->string('city');
            $table->foreignId('country_id')->constrained('countries');
            $table->string('language'); // only en and nl?
            $table->boolean('is_active')->default(false);
            $table->boolean('studying')->default(true);
            $table->foreignId('study_phase_id')->nullable()->constrained()->nullOnDelete();
            $table->boolean('alive')->nullable()->default(true); //why??
            $table->year('registration_year')->useCurrent()->nullable();
            $table->string('iban')->nullable();
            $table->string('bic')->nullable();
            $table->string('bank_account_name')->nullable();
            $table->string('bank_account_city')->nullable();
            $table->string('parents_email')->nullable();
            $table->string('parents_phone_number')->nullable();
            $table->string('password')->nullable(); // passwords?
            $table->rememberToken();
            $table->timestamp('last_login')->useCurrent();
            $table->string('role')->default('user'); // Should this be a separate pivot table?
            $table->timestamps();
            // should we have columns for parents info?
            // should we have columns for OV?
            // TODO: add columns for smoelenboek info sharing
        });

        Schema::create('password_reset_tokens', function (Blueprint $table) {
            $table->string('email')->primary();
            $table->string('token');
            $table->timestamp('created_at')->nullable();
        });

        Schema::create('sessions', function (Blueprint $table) {
            $table->string('id')->primary();
            $table->foreignId('user_id')->nullable()->index();
            $table->string('ip_address', 45)->nullable();
            $table->text('user_agent')->nullable();
            $table->longText('payload');
            $table->integer('last_activity')->index();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('users');
        Schema::dropIfExists('password_reset_tokens');
        Schema::dropIfExists('sessions');
    }
};
