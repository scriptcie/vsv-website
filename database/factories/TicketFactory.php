<?php

namespace Database\Factories;

use App\Models\Event;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Ticket>
 */
class TicketFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        // Get a random event for which the ticket is created
        $event = Event::inRandomOrder()->first();
        // Get a random user that buys the ticket
        $user = User::inRandomOrder()->first()->toArray();
        // If event is public, then the ticket can also be bought by non-logged in user
        if ($event->public == 1) {
            $user = $this->faker->randomElement([$user, null]);
        }
        // If the event has fields than we can add some data as well
        // Note that fields specific to certain event options are ignored here
        // as the options can only be added at a later stage
        if ($fields = $event->fields()->whereNull('event_option_id')->get()) {
            $data = $fields->mapWithKeys(function ($field) {
                return [$field->name => $this->faker->words(3, true)];
            });
        }

        return [
            'event_id' => $event->id,
            'user_id' => $user['id'] ?? null,
            'email' => $user['email'] ?? $this->faker->email(),
            'data' => $data ?? null,
            'token' => Str::uuid(),
            'checked_at' => $this->faker->randomElement([null, null, null, null, Carbon::now()->toDateTimeString()]),
        ];
    }
}
