<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\user>
 */
class UserFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $gender = $this->faker->randomElement([1, 2, 3]);
        $first_name = $this->faker->firstName($gender);

        return [
            'student_number' => $this->faker->unique()->numberBetween(4000000, 7000000),
            'initials' => initials($first_name),
            'first_name' => $first_name,
            'last_name' => $this->faker->lastName,
            'gender_id' => $gender,
            'birthday' => $this->faker->dateTimeBetween('-30 years', '-20 years')->format('Y-m-d H:i:s'),
            'birthplace' => $this->faker->city,
            'nationality_id' => $this->faker->numberBetween(1, 200),
            'email' => $this->faker->unique()->safeEmail,
            'email_verified_at' => now(),
            'phone_number' => sprintf('+31 6%s %s', $this->faker->randomNumber(2, true), $this->faker->randomNumber(6, true)),
            'address' => sprintf('%s %s', $this->faker->streetName, $this->faker->buildingNumber),
            'postal_code' => sprintf('%s%s', $this->faker->randomNumber(4, true), strtoupper($this->faker->randomLetter.$this->faker->randomLetter)),
            'city' => $this->faker->city,
            'country_id' => 157,
            'language' => $this->faker->randomElement(['en', 'nl']),
            'is_active' => true,
            'study_phase_id' => $this->faker->numberBetween(1, 4),
            'registration_year' => $this->faker->dateTimeBetween('-8 years', 'today')->format('Y'),
            'iban' => $this->faker->iban('NL'),
            'bic' => $this->faker->swiftBicNumber,
            'parents_email' => $this->faker->unique()->safeEmail,
            'parents_phone_number' => sprintf('+31 6%s %s', $this->faker->randomNumber(2, true), $this->faker->randomNumber(6, true)),
            'password' => bcrypt('password'),
            'remember_token' => Str::random(10),
        ];
    }

    public function admin(): Factory
    {
        return $this->state(function (array $attributes) {
            return [
                'role' => 'admin',
            ];
        });
    }

    public function board(): Factory
    {
        return $this->state(function (array $attributes) {
            return [
                'role' => 'board',
            ];
        });
    }
}
