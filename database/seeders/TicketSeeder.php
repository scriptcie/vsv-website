<?php

namespace Database\Seeders;

use App\Models\Ticket;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class TicketSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        // Then create tickets using factory incl. payments
        $tickets = Ticket::factory()->count(20)->create();
        // But as tickets are pretty much useless without options we need to add those to the tickets
        foreach ($tickets as $ticket) {
            $ticket_base_option = $ticket->event->options()->where('base', 1)->inRandomOrder()->first();

            if (! $ticket_base_option) {
                continue;
            }

            $ticket_base_option_id = $ticket_base_option->id;

            $ticket_other_option_id = array_rand([
                optional($ticket->event->options()->where('base', 0)->inRandomOrder()->first()),
                null,
            ]);
            $ticket_option_ids = array_filter([$ticket_base_option_id, $ticket_other_option_id]);
            $ticket->options()->attach($ticket_option_ids);
        }
    }
}
