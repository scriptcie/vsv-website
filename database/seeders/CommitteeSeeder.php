<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CommitteeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('committees')->upsert([
            ['name' => 'Board', 'abbreviation' => 'Board', 'email' => 'vsv@tudelft.nl', 'description' => 'The board consists of 7 students, who are a full-time member of the board of our society and put their study on hold for a year to fully dedicate themselves to the society.'],
            ['name' => 'Script Commissie', 'abbreviation' => 'ScriptCie', 'email' => 'scriptcie-vsv@tudelft.nl', 'description' => 'The Script Committee consists of a group of enthusiastic students who are responsible for all IT-solutions of the VSV \'Leonardo da Vinci\'. They make sure the website keeps working properly and they build ingenious programs like the VSV ticketshop!'],
            ['name' => 'Activiteiten Commissie', 'abbreviation' => 'AkCie', 'email' => null, 'description' => null],
            ['name' => 'Aerospace Diversity Department', 'abbreviation' => 'ADD', 'email' => null, 'description' => null],
            ['name' => 'Airbase', 'abbreviation' => 'Airbase', 'email' => null, 'description' => null],
            ['name' => 'Atmosfeer Lustrum Group', 'abbreviation' => 'ALG', 'email' => null, 'description' => null],
        ], ['name']);

        // DB::table('committees')->where('id', 3)->update(['is_active' => false]);
    }
}
