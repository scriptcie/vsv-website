<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GenderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('genders')->upsert([

            ['name' => 'Male', 'abbreviation' => 'm'],
            ['name' => 'Female', 'abbreviation' => 'f'],
            ['name' => 'Other', 'abbreviation' => 'x'],
        ], ['name']);
    }
}
