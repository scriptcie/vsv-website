<?php

namespace Database\Seeders;

use App\Models\Committee;
use App\Models\Event;
use App\Models\EventOption;
use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EventSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $now = Carbon::now();

        // Create events
        DB::table('events')->upsert([
            [
                'id' => 1,
                'title' => 'Belgian Beer Drink',
                'description' => 'An evening with lots of good Belgian beers and live music.',
                'location' => 'Societeit Phoenix',
                'start' => $now->copy()->hour(19)->minute(0)->second(0)->addDays(18),
                'end' => $now->copy()->hour(3)->minute(0)->second(0)->addDays(19),
                'committee_id' => Committee::where('abbreviation', 'AkCie')->first()->value('id'),
                'capacity' => 300,
                'public' => true,
            ],
            [
                'id' => 2,
                'title' => 'VSV Symposium',
                'description' => 'The annual symposium of the VSV \'Leonardo da Vinci\'.',
                'location' => 'Aula TU Delft',
                'start' => $now->copy()->addDays(30)->hour(9)->minute(0)->second(0),
                'end' => $now->copy()->addDays(30)->hour(17)->minute(0)->second(0),
                'committee_id' => Committee::where('abbreviation', 'Board')->first()->value('id'),
                'capacity' => 500,
                'public' => true,
            ],
            [
                'id' => 3,
                'title' => 'InterCom Silent Disco',
                'description' => 'A silent disco inside De Atmosfeer.',
                'location' => 'De Atmosfeer',
                'start' => $now->copy()->addDays(3)->hour(20)->minute(0)->second(0),
                'end' => $now->copy()->addDays(4)->hour(1)->minute(0)->second(0),
                'committee_id' => Committee::where('abbreviation', 'Board')->first()->value('id'), // TODO: Change this to actually link to a correct id
                'capacity' => 100,
                'public' => true,
            ],
            [
                'id' => 4,
                'title' => 'New Website Launch Party',
                'description' => 'The launch party of the new VSV website.',
                'location' => 'Top-secret location',
                'start' => $now->copy()->addDays(80)->hour(20)->minute(0)->second(0),
                'end' => $now->copy()->addDays(80)->hour(23)->minute(0)->second(0),
                'committee_id' => Committee::where('abbreviation', 'ScriptCie')->first()->value('id'),
                'capacity' => 80,
                'public' => false,
            ],
        ], ['id']);

        // Add options
        $event_id_BBD = Event::where('title', 'Belgian Beer Drink')->orderByDesc('id')->first()->id;
        $event_id_VSVS = Event::where('title', 'VSV Symposium')->orderByDesc('id')->first()->id;
        $event_id_ICSD = Event::where('title', 'InterCom Silent Disco')->orderByDesc('id')->first()->id;
        $event_id_NWLP = Event::where('title', 'New Website Launch Party')->orderByDesc('id')->first()->id;
        DB::table('event_options')->insert([
            ['event_id' => $event_id_BBD, 'name' => 'Early-bird entry', 'price' => 10, 'base' => true, 'buyable' => true, 'capacity' => 10],
            ['event_id' => $event_id_BBD, 'name' => 'Entry', 'price' => 7.5, 'base' => true, 'buyable' => true, 'capacity' => null],
            ['event_id' => $event_id_BBD, 'name' => '5 coins', 'price' => 10, 'base' => false, 'buyable' => true, 'capacity' => null],
            ['event_id' => $event_id_BBD, 'name' => '10 coins', 'price' => 15, 'base' => false, 'buyable' => true, 'capacity' => null],
            ['event_id' => $event_id_NWLP, 'name' => 'Entry for ScriptCie members', 'price' => null, 'base' => true, 'buyable' => true, 'capacity' => null],
            ['event_id' => $event_id_NWLP, 'name' => 'Entry for non-ScriptCie members', 'price' => 5, 'base' => true, 'buyable' => true, 'capacity' => 40],
            // TODO: add more options
        ]);

        // Add special fields
        $event_option_scriptcie = EventOption::where('event_id', $event_id_NWLP)->where('name', 'Entry for ScriptCie members')->orderByDesc('id')->first()->id;
        DB::table('event_fields')->insert([
            ['event_id' => $event_id_NWLP, 'name' => 'Passphrase', 'description' => 'Please the passphrase to make sure that you are a ScriptCie member.', 'html' => null, 'validation' => 'required|string|size:4', 'event_option_id' => $event_option_scriptcie],
            ['event_id' => $event_id_NWLP, 'name' => 'Favorite programming language', 'description' => 'Please select your favorite programming language.', 'html' => '<select name="data[3]" class="form-select"><option value="php">PHP</option><option value="python">Python</option></select>', 'validation' => 'required|in:php,python', 'event_option_id' => null],
        ]);
    }
}
