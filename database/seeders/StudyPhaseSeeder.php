<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class StudyPhaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('study_phases')->upsert([
            ['name' => 'Bachelor'],
            ['name' => 'Master'],
            ['name' => 'Minor'],
            ['name' => 'Exchange'],
            ['name' => 'Bridging Programme'],
            ['name' => 'N/A'],
        ], ['name']);
    }
}
