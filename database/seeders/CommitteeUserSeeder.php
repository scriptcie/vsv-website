<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CommitteeUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('committee_user')->upsert([

            ['id' => 1, 'user_id' => '1', 'committee_id' => '2', 'function' => 'Jester', 'year' => date('Y'), 'rank' => '69'],
            ['id' => 2, 'user_id' => '3', 'committee_id' => '2', 'function' => 'Koning', 'year' => date('Y'), 'rank' => '70'],
            ['id' => 3, 'user_id' => '4', 'committee_id' => '1', 'function' => 'Koning', 'year' => date('Y'), 'rank' => '70'],
            ['id' => 4, 'user_id' => '5', 'committee_id' => '3', 'function' => 'Koning', 'year' => date('Y'), 'rank' => '70'],
            ['id' => 5, 'user_id' => '6', 'committee_id' => '4', 'function' => 'Koning', 'year' => date('Y'), 'rank' => '70'],
        ], ['id']);
    }
}
