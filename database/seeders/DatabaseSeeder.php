<?php

namespace Database\Seeders;

use App\Models\User;
// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        $this->call([
            CountrySeeder::class,
            GenderSeeder::class,
            StudyPhaseSeeder::class,
            CommitteeSeeder::class, // TODO: add all committees
        ]);

        if (config('app.env') == 'production') {
            return;
        }

        $this->call([
            UserSeeder::class,
            EventSeeder::class,
            CommitteeUserSeeder::class,
            TicketSeeder::class,
        ]);
    }
}
