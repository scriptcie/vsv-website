in `\app\Helpers\general.php` you can find some helpers functions.
this file is auto loaded using composer.

the following functions exist:

| function | explanation |
| ---- | ---- |
| `initials($name)` | gets the initials of any name |
| `qr_code($content)` | generates a QR-Code |
| ... | ... |
