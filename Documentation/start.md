# Overview

I highly recommend to [watch the 30 Days to Learn Laravel](https://youtube.com/playlist?list=PL3VM-unCzF8hy47mt9-chowaHNjfkuEVz&si=8vocpfWALKrj7Eqo) from Laracast. Or if you have less time then [everything you need to know about laravel in 30 min](https://www.youtube.com/watch?v=e7z6KJkGhmg)

## How to install/run

- [ ] TODO: Add this entire section

## Front end

The front end is build up using [Laravel Blade templates](https://laravel.com/docs/11.x/blade#main-content), and uses [Bootstrap](https://getbootstrap.com/) for styling, in this section the front end is explained.

### Blade Templates

Blade is a templating engine that allows us developers to write dynamic and reusable views. This allows us to reuse certain parts of the website, and change what is visible depending on who is logged in.
In this project blade is used to split components like the navbar, and footer into separate files, making it easy to change and maintain the front end of the website.
More on blade can be found in the [Laravel Blade template docs](https://laravel.com/docs/11.x/blade#main-con), and more on the structure of the blade files in this project can be found [here](frontend/bladestructure.md)


### Bootstrap

Bootstrap is a popular front-end framework, that provides pre-styled components and a responsive grid system. It was installed using npn, the JavaScript part is included via the [bootstrap.js](../resources/js/bootstrap.js) file, while for the css, the sass folder was copied into the `resources/sass` folder, and then it was included into the [app.scss](../resources/sass/app.scss) file. Most of the components are included via html classes, but some parts require specific attributes, for more information consult the [bootstrap docs](https://getbootstrap.com/docs/5.3/getting-started/introduction/)


### custom css

Bootstrap alone is not enough to make this website unique, therefore some custom css was written, in [app.scss](../resources/sass/app.scss) the fonts and most colors are changed. While in [app.css](../resources/css/app.css) all custom css classes are written. 

- [ ] TODO: Document the custom css

### Font Awesome

Font Awesome is an awesome icon pack, used for all icons on the website. It is installed using npm, and included via the [app.scss](../resources/sass/app.scss) file. To use a Font Awesome icon, simply look through [Their website](https://fontawesome.com/search?ic=free) to find an icon you like and paste the html into the location where you want to use it:

```html
<i class="fa-solid fa-users"></i>
```


## Back end

The back end is all Laravel, connected to a `insert database`. For development a SQLite database is used instead.

### Database

For the structure of the database, reference the following image:

- [ ] TODO: Add structure

### Models

- [ ] TODO: Add explanation abut what everything is.
- [ ] TODO: Say if the seeder is for dev only, or not. (same for factories)

The goal of the next sections is to be able to quickly find where to look for stuff.

#### Committee

Committees are used for showing all committees, all members of committees, and every event is 'owned' by a committee. 

has a `migration`, `seeder`, `resource controller`, 
and a pivot with the `user` model 


#### Event

Events are more complex, and there are 3 models `Event`, `EventOptions`, and `Event Fields`.
Events are the events itself, logically.
Event Options are the options a user can select when signing up for a event.
Event Fields are for if an event options should not be a checkbox. (NOT CONFIDENT ON THIS).


Events has a `migration`, `seeder`, `resource controller`, `Policy`

Event Options has a `migration`, and uses the `EventSeeder`

Event Fields has a `migration`, and also uses the `EventSeeder`

The Event Options and Event Fields are related using a pivot table

#### Ticket

Ticket has a `migration`, `seeder`, `controller`, `policy`, `factory`

#### Users

Users are more complex, and the Country, Gender, and Study phase are split up into their models.

User has a `migration`, `seeder`, `controller`, `factory`

Country, Gender, and study phases all have `migration`, `seeder`

### Helper functions

found in [app/Helpers/general.php](../app/Helpers/general.php). Helper functions are ... .

An overview of helpers functions can be found [here](backend/helpers.md)

### Tests

- [ ] TODO: Write the tests and documentation for them.
