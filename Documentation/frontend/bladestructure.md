# Structure of blade files

All blade files can be found in the `resources/views/` folder, where all the overall layout blade files are located in the layout folder

All views expand the [base.blade.php](../../resources/views/layout/base.blade.php). The topbar, where the user can login/logout is located in the [topbar.blade.php](../../resources/views/layout/topbar.blade.php) file. The navigation bar is located in [navbar.blade.php](../../resources/views/layout/navbar.blade.php) file. The success and error callouts are located in the [success.blade.php](../../resources/views/layout/success.blade.php) and [error.blade.php](../../resources/views/layout/error.blade.php) files respectively. And finally the footer is located in the [footer.blade.php](../../resources/views/layout/footer.blade.php).

To create a new view using this layout, you should extend the base file in the following way: 

```php
@extends('layout.base')

@section('title', 'Title of the view')

@section('main')
    
    // Place content here  

@endsection

```
