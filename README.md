 The website of the VSV Leonardo da Vinci

![https://gitlab.styleci.io/repos/22724802](https://gitlab.styleci.io/repos/22724802/shield)
![https://gitlab.com/scriptcie/vsv-website/-/pipelines](https://gitlab.com/scriptcie/vsv-website/badges/master/pipeline.svg?style=flat-square)
![https://gitlab.com/scriptcie/vsv-website/-/graphs/master/charts](https://gitlab.com/scriptcie/vsv-website/badges/master/coverage.svg?style=flat-square)

This is where the magic happens. 
Here you find all the code that powers the website of the VSV Leonardo da Vinci. B'vo!

This repository is maintained by the [ScriptCie](mailto:scriptcie-vsv@tudelt.nl) of the very best SV.

## History

The first version of the VSV website consisted just of some good old static HTML & CSS files. The world was still rather analog back then...

The second version was deployed around the year 2013 and was based on Drupal. This site was a major achievement and looked extremely neat. A ticketshop and pages for members of the VSV were developed subsequently. These applications proofed very successful. However, the site was a hell to maintain and the ScriptCie members that initially developed and understood the overall structure were gone at some point.

The third version of the VSV website is now under active development and planned to be released soon. When you are reading this, chances are high that you are involved in the development of this brand-new version. Welcome young ScriptCie padawan!

## Architecture

For more specific information look at the full [documentation](Documentation/start.md).

This website is a [Laravel](https://laravel.com/) website. For the frontend [Bootstrap](https://getbootstrap.com/), and some custom css is used.

## Installation

## Contribute

## Security Vulnerabilities

If you discover a security vulnerability within the code of the VSV website code, please send an e-mail to [vsv@tudelft.nl](mailto:vsv@tudelft.nl). All security vulnerabilities will be promptly addressed.

## License

The VSV website is open-sourced software licensed under the MIT license. Please see [LICENSE](LICENSE.md) for details.

